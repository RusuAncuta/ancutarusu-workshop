package ro.wantsome;


import ro.wantsome.DctionaryOperations.*;
import ro.wantsome.FileReader.GlobalInputFileReader;
import ro.wantsome.FileReader.InputFileReader;
import ro.wantsome.FileReader.ResourceInputFileReader;
import ro.wantsome.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Main {

    public static void main(String[] arg) throws IOException {
        List<String> allLines = new ArrayList<>();
        //InputFileReader inputFileReader= new ResourceInputFileReader();
        //allLines = inputFileReader.readFile("dex.txt");
        InputFileReader inputFileReader=new GlobalInputFileReader();
        allLines=inputFileReader.readFile("C:\\CODE\\ancutarusu-workshop\\src\\main\\resources\\dex.txt");
        //aduce toate informatiile din dex file

        Set<String> wordsSet = Utils.removeDuplicates(allLines);
        //facem astfel incat sa putem da noi un parametru din consola cu Scanner
        //Scanner in = new Scanner(System.in);
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            //creaza un meniu
            System.out.println("Menu:\n\t" +
                    "1. Search\n\t" +
                    "2. Find all Palindroms\n\t" +
                    "3. Load random word\n\t"+
                    "4. Find anagrams\n\t"+
                    "0. Exiting");
            String userMenuSelection = in.readLine();

            //Exit if user selected 0
            if(userMenuSelection.equals("0")){
                System.out.println("Exiting....");
                break;//ca sa iasa din meniu
            }
            //operation o  initializam cu null
                DictionaryOperation operation=null;
            switch (userMenuSelection) {
                //va face call la fidWordsThatContains din Utils
                case "1":

                    System.out.println("Searching");
                    operation=new SearchDictionaryOperation(wordsSet,in);

                    break;

                case "2":
                    operation=new FindPalindroms(wordsSet);

                    break;
                case "3":
                    operation=new RandomWordDictionaryOperation(wordsSet);
                    break;
                case "4":
                    operation=new SearchAnagrams(allLines, in);
                    break;

                default:
                    System.out.println("Please select a valid option");
            }
            if(operation!=null){
                operation.run();
            }

        }
    }
}





