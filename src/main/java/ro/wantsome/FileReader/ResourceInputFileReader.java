package ro.wantsome.FileReader;

import ro.wantsome.Main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class ResourceInputFileReader implements InputFileReader {
    @Override
    public List<String> readFile(String input) throws IOException {
        List<java.lang.String> result = new ArrayList<>();
        ClassLoader classLoader = Main.class.getClassLoader();
        URL resource = classLoader.getResource(input);
        File file = null;
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {
            file = new File(resource.getFile());
        }

        try (FileReader reader = new FileReader(file);
             BufferedReader br = new BufferedReader(reader)) {

            String line;
            while ((line = br.readLine()) != null) {
                result.add(line);
            }
        }
        return result;
    }
    }

