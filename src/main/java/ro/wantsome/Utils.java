package ro.wantsome;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.Arrays;

public class Utils {
    /**
     * Can read a given file from the resources folder
     *
     * @param fileName file name
     * @return list of all the lines in the file
     */

    /**
     * Can read a given file in the working directory
     *
     * @param fileName file name
     * @return list of all the lines in the file
     */
    public static List<String> readAllLinesFromSourceFile(String fileName) throws IOException {
        return Files.readAllLines(Paths.get(fileName));
    }

    public static Set<String> removeDuplicates(List<String> allLines) {
        Set<String> wordsSet = new HashSet<>();
        for (String line : allLines) {
            wordsSet.add(line);
        }
        return wordsSet;
    }

    public static Set<String> findWordsThatContain(Set<String> wordsSet, String word) {

        Set<String> result = new HashSet<>();
        for (String line : wordsSet) {
            if (line.contains(word)) {
                result.add(line);
            }
        }
        return result;
    }

    public static Set<String> findPalindromes(Set<String> wordList) {
        Set<String> result = new HashSet<>();
        //sets the exception
        if (wordList == null) {
            System.out.println("warning null parameter in palindroms");
            return result;
        }
        for (String line : wordList) {
            StringBuilder reverseWord = new StringBuilder(line).reverse();
            if (line.equals(reverseWord.toString()))
                // append a string into StringBuilder input1
                result.add(line);

        }
        return result;
    }

    //sort the letters in a word alphabetically
    public static String sortingLetters(String word) {
        //if (word != null) {
            char[] sortedW = word.toCharArray();
            Arrays.sort(sortedW);
            return new String(sortedW);
        //} else return "warning-----> null parameter";
    }

    //create the anagrams list
    public static List<String> anagramList(List<String> wordList, String word) {
        List<String> anagramList = new ArrayList<>();
        for (String e : wordList) {
            if (sortingLetters(word).equals(sortingLetters(e)) && e.length() == word.length()) {
                anagramList.add(e);
            }
        }
        return anagramList;
    }

    public static Map<String, Set<String>> mapWithAnagrams(List<String> wordList, String word) {
        Map<String, Set<String>> anagramsMap = new HashMap<>();

        Set<String> anagramsList = new TreeSet<>(Utils.anagramList(wordList, word));

        anagramsMap.put(sortingLetters(word), anagramsList);
        return anagramsMap;

    }

    public static List<String> sortSet(Set<String> unsorted) {
        List<String> result = new ArrayList<>(unsorted);
        Collections.sort(result);
        return result;
    }

    public static String getrandomWord(Set<String> wordsSet) {
        //ar trebui sa ne folosim de un index pt a putea extrage cuvantul
        List<String> wordList = new ArrayList<>(wordsSet);
        //using random class, find a way to extract a random word from the set
        Random random = new Random();
        int index = random.nextInt(wordList.size());//iti da o valoare de la 0 la 19

        return wordList.get(index);
    }

    public static String getRandomWord(Set<String> wordSet) {
        Random random = new Random();
        return getRandomWordFromSet(wordSet, random);
    }

    public static String getRandomWordFromSet(Set<String> wordSet, Random random) {
        List<String> wordList = new ArrayList<>(wordSet);
        int index = random.nextInt(wordList.size());
        return wordList.get(index);
    }
}




