package ro.wantsome.DctionaryOperations;

import ro.wantsome.Utils;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class RandomWordDictionaryOperation implements DictionaryOperation{
    private Set<String> wordsSet;

    public RandomWordDictionaryOperation(Set<String> wordsSet){
        this.wordsSet=wordsSet;

    }
    @Override
    public void run() throws IOException {
        Set<String> randomWord = Collections.singleton(Utils.getrandomWord(wordsSet));
            System.out.println(randomWord);
        }
    }

