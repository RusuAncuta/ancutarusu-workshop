package ro.wantsome.DctionaryOperations;

import ro.wantsome.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class SearchDictionaryOperation implements DictionaryOperation {
    private Set<String> wordsSet;
    private BufferedReader in;


    public SearchDictionaryOperation(Set<String> wordsSet, BufferedReader in){
        this.in=in;
        this.wordsSet=wordsSet;
    }

    @Override
    public void run() throws IOException {
        //to do
        String userGivenWord = in.readLine();

        if ("".equals(userGivenWord))
            return;//or exit(0)
        Set<String> resultedWords = Utils.findWordsThatContain(wordsSet, userGivenWord);

        List<String> sortedWord=Utils.sortSet(resultedWords);

        Collections.sort(sortedWord);
        for (String line : sortedWord) {
            System.out.println(line);
        }
    }
}
