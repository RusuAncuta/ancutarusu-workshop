package ro.wantsome.DctionaryOperations;

import ro.wantsome.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SearchAnagrams implements DictionaryOperation {
        private List<String> wordSet;
        private BufferedReader in;

        public SearchAnagrams(List<String> wordSet, BufferedReader in){
            this.wordSet=wordSet;
            this.in = in;
        }

    @Override
    public void run() throws IOException {
            String word=in.readLine();
        Map<String, Set<String>> anagramMap= Utils.mapWithAnagrams(wordSet,word);
        for(Map.Entry a: anagramMap.entrySet()){
            System.out.println(a);
        }

    }
}
