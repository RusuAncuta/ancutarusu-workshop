package ro.wantsome.DctionaryOperations;

import ro.wantsome.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.*;

public class AnagramsDictionary implements DictionaryOperation {
    private Set<String> wordSet;
    private BufferedReader in;

    public AnagramsDictionary(){this.wordSet=wordSet;
    this.in=in;}


    @Override
    public void run() throws IOException {
        System.out.println("Finding Anagrams");
        String userGivenWord=in.readLine();

        Map<String, List<String>> lettersVSAnagrams=new HashMap<>();

        for (String word:wordSet){
            String sortedLetters= Utils.sortingLetters(word);

            if(lettersVSAnagrams.containsKey(sortedLetters)){
                lettersVSAnagrams.get(sortedLetters).add((word));
                            }
            else{
                List<String> anagrams=new ArrayList<>();
                anagrams.add((word));
                lettersVSAnagrams.put(sortedLetters,anagrams);
            }

        }
        List<String>desiredAnagrams=lettersVSAnagrams.get(Utils.sortingLetters(userGivenWord));
        if(desiredAnagrams==null || desiredAnagrams.size()<2){
            System.out.println("The word does not have any anagrams in dictionary");
            return;
        }
        Collections.sort(desiredAnagrams);
        System.out.println(desiredAnagrams);
    }
}
