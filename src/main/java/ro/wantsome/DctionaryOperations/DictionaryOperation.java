package ro.wantsome.DctionaryOperations;

import java.io.IOException;

public interface DictionaryOperation {
    void run() throws IOException;
}
