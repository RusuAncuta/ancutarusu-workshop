package ro.wantsome.DctionaryOperations;

import ro.wantsome.Utils;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public class FindPalindroms implements DictionaryOperation{
    private Set<String> wordsSet;

    public FindPalindroms(Set<String> wordsSet){
        this.wordsSet=wordsSet;

    }
    @Override
    public void run() throws IOException {
        System.out.println("Palindromes");
        Set<String> palindromes = Utils.findPalindromes(wordsSet);

        List<String> sortedPalindromes=Utils.sortSet(palindromes);
        for (String line : palindromes) {
            System.out.println(line);
        }
    }
}
